<?php
	/*
	Template Name: Mẫu Liên hệ
	*/
?>

<?php get_header(); ?>

<?php
    $page_id        = get_the_ID();
    $page_name      = get_the_title();
    $page_content   = wpautop(get_the_content());

    //field
    $customer_address   = get_field('customer_address', 'option');
    $customer_phone     = get_field('customer_phone', 'option');
    $customer_email     = get_field('customer_email', 'option');

    $contact_page_title = get_field('contact_page_title');

    $contact_contact_form_id    = get_field('contact_contact_form');
    $contact_contact_form       = do_shortcode('[contact-form-7 id="'.$contact_contact_form_id.'"]');

    $contact_map = get_field('contact_map');
?>

<?php get_template_part("resources/views/page-banner"); ?>

<div class="contact-us pt-60 pb-50">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="contact-page-title mb-40">
                    <h1>
                        <?php echo $contact_page_title; ?>
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <ul class="contact-tab-list nav">
                    <li><a href="#contact-address" data-toggle="tab">Liên hệ</a></li>
                    <li><a href="#contact-form-tab" data-toggle="tab">Để lại tin nhắn</a></li>
                    <li><a class="active" href="#store-location" data-toggle="tab">Địa chỉ</a></li>
                </ul>
            </div>
            <div class="col-lg-8">
                <div class="tab-content tab-content-contact">
                    <div id="contact-address" class="tab-pane fade row d-flex">
                        <div class="col-lg-4 col-md-4">
                            <div class="contact-information">
                                <h4>Địa chỉ</h4>
                                <p><?php echo $customer_address; ?></p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="contact-information mrg-top-sm">
                                <h4>Điện thoại</h4>
                                <p>
                                    <a href="tel:<?php echo str_replace(' ','',$customer_phone);?>">
                                        <?php echo $customer_phone; ?></a>
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="contact-information mrg-top-sm">
                                <h4>Email</h4>
                                <p>
                                    <a href="mailto:<?php echo $customer_email; ?>"><?php echo $customer_email; ?></a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div id="contact-form-tab" class="tab-pane fade row d-flex">
                        <div class="col">
                            <?php if(!empty( $contact_contact_form )) { ?>
                                <?php echo $contact_contact_form; ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div id="store-location" class="tab-pane fade row d-flex active show">
                        <div class="col-12">
                            <div class="contact-map">
                                <?php echo $contact_map; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
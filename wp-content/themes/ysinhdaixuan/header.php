<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="canonical" href="<?php echo get_page_link_current(); ?>" />
    <title><?php echo title(); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>


<script type="text/javascript">
    var ajax_url = '<?php echo admin_url('admin-ajax.php'); ?>';
</script>


<?php
    //field
    $customer_phone = get_field('customer_phone', 'option');
?>

<header class="header-area">
    <div class="header-middle header-middle-color-3 ptb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="logo logo-mrg">
                        <!-- logo -->
                        <?php get_template_part("resources/views/logo"); ?>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="header-contact-search-wrapper f-right">
                        <div class="header-contact middle-same">
                            <div class="header-contact-icon">
                                <i class="pe-7s-headphones"></i>
                            </div>
                            <div class="header-contact-content">
                                <p>
                                    <span class="d-md-inline-block d-none">Liên hệ</span> <br>Hỗ trợ miễn phí: 
                                    <a href="tel:<?php echo str_replace(' ','',$customer_phone);?>" title="">
                                        <?php echo $customer_phone; ?>
                                    </a>
                                </p>
                            </div>
                        </div>
                        <div class="d-flex align-items-center">
                            <!-- search -->
                            <?php get_template_part("resources/views/search-form"); ?>

                            <!-- cart -->
                            <?php get_template_part("resources/views/wc/wc-info-cart"); ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom header-bottom-color-2 theme-bg-2">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="logo mobile-logo">
                        <!-- logo mobile -->
                        <?php get_template_part("resources/views/logo"); ?>
                    </div>
                </div>

                <div class="col-12">
                    <div class="mobile-menu-area">
                        <div class="mobile-menu">
                            <div id="mobile-menu-active">
                                <!-- menu mobile -->
                                <?php get_template_part("resources/views/menu"); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="main-menu main-none">
                <!-- menu -->
                <?php get_template_part("resources/views/menu"); ?>
            </div>
        </div>
    </div>
</header>
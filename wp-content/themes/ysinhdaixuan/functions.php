<?php
include_once get_template_directory(). '/load/Custom_Functions.php';
// include_once get_template_directory(). '/load/CTPost_CTTax.php';
include_once get_template_directory(). '/load/Performance.php';
include_once get_template_directory(). '/load/wc.php';
include_once get_template_directory(). '/resources/widgets/show-list-category.php';
// include_once get_template_directory(). '/resources/widgets/show-list-tag.php';
include_once get_template_directory(). '/resources/widgets/show-related-post.php';


/* Create CTPost */
// (title, slug_code, slug)
// create_post_type("Sản phẩm","product","san-pham");
/* Create CTTax */
// (title, slug, slug_code, post_type)
// create_taxonomy_theme("Danh mục Sản phẩm","danh-muc-san-pham","product_cat","product");


// Create menu Theme option use Acf Pro
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title'    => 'Tuỳ chỉnh',      // Title hiển thị khi truy cập vào Options page
        'menu_title'    => 'Tuỳ chỉnh',      // Tên menu hiển thị ở khu vực admin
        'menu_slug'     => 'theme-settings', // Url hiển thị trên đường dẫn của options page
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
}


// Title Head Page
if (!function_exists('title')) {
    function title()
    {
        if (is_home() || is_front_page()) {
            return get_option('blogname') .' - '. get_option('blogdescription');
        }

        if (is_archive()) {
            $obj = get_queried_object();
            $return = $obj->name;
                if($return == 'product'){
                    $return_kq = __( 'Sản phẩm', 'text_domain' );
                }else{
                    $return_kq = $return;
                }
            return $return_kq;
        }

        if (is_search()) {
            return __( 'Tìm kiếm cho', 'text_domain' ).' : ['.$_GET['s'].']';
        }

        if (is_404()) {
            return __( '404 Không tìm thấy trang', 'text_domain' );
        }

        return get_the_title();
    }
}


// Url File theme
if (!function_exists('asset')) {
    function asset($path)
    {
        return wp_slash(get_stylesheet_directory_uri() . '/dist/' . $path);
    }
}


// Url image theme
if (!function_exists('getPostImage')) {
    function getPostImage($id, $imageSize = '')
    {
        $img = wp_get_attachment_image_src(get_post_thumbnail_id($id), $imageSize);
        return (!$img) ? asset('images/no-image-wc.png') : $img[0];
    }
}


// Cut String text
if (!function_exists('cut_string')) {
    function cut_string($str,$len,$more){
        if ($str=="" || $str==NULL) return $str;
        if (is_array($str)) return $str;
            $str = trim(strip_tags($str));
        if (strlen($str) <= $len) return $str;
            $str = substr($str,0,$len);
        if ($str != "") {
            if (!substr_count($str," ")) {
              if ($more) $str .= " ...";
              return $str;
            }
            while(strlen($str) && ($str[strlen($str)-1] != " ")) {
                $str = substr($str,0,-1);
            }
            $str = substr($str,0,-1);
            if ($more) $str .= " ...";
        }
        return $str;
    }
}


// Get url page here
if (!function_exists('get_page_link_current')) {
    function get_page_link_current(){
        // Get url page here
        $page_link_current_get = sprintf(
            "%s://%s%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME'],
            $_SERVER['REQUEST_URI']
        );

        // Get url, exclude url GET
        $page_link_current_strstr = strstr($page_link_current_get, '?');
        $page_link_current        = str_replace( $page_link_current_strstr, '', $page_link_current_get );

        return $page_link_current;
    }
}


// Get url page template by name file (vd : template-contact.php)
if (!function_exists('get_link_page_template')) {
    function get_link_page_template($name_file){
        $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => $name_file
        ));
        
        $page_template_link = get_page_link($pages[0]->ID);

        return $page_template_link;
    }
}


// Pagination
function paginationCustom($max_num_pages) {
    echo '<div class="pagination-style"><ul>';
    if ($max_num_pages > 1) {   // tổng số trang (10)
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; // trang hiện tại (8)

        if ($max_num_pages > 9) {
            echo '<li class=""><a href="'.esc_url( get_pagenum_link( 1 ) ).'" class="item">
            ...</a></li>';
        }
        if ($paged > 1) {
            echo '<li class=""><a href="'.esc_url( get_pagenum_link( $paged - 1 ) ).'" class="item prev-next prev">
            <i class="ion-ios-arrow-left"></i> Prev</i></a></li>';
        }
        if ($paged >= 5 && $max_num_pages > 9) {
            echo '<li class=""><a href="'.esc_url( get_pagenum_link( $paged - 5 ) ).'" class="item">
            -5</a></li>';
            echo '<li class=""><a href="javascript:void(0)" class="">&nbsp;</a></li>';
        }

        for($i= 1; $i <= $max_num_pages; $i++) {
            // $half_total_links = floor( 5 / 2);
            $half_total_links = 2;

            $from = $paged - $half_total_links; // trang hiện tại - 2 (8-2= 6)
            $to = $paged + $half_total_links;   // trang hiện tại + 2 (8+2 = 10)

            if ($from < $i && $i < $to) {   // $form cách $to 3 số (từ 6 đến 10 là 7,8,9)
                $class = $i == $paged ? 'active' : 'item';
                echo '<li class=" "><a href="'.esc_url( get_pagenum_link( $i ) ).'" class="'.$class.'">'.$i.'</a></li>';
            }
        }

        if ($paged <= $max_num_pages - 5 && $max_num_pages > 9) {
            echo '<li class=""><a href="javascript:void(0)" class="">&nbsp;</a></li>';
            echo '<li class=""><a href="'.esc_url( get_pagenum_link( $paged + 5 ) ).'" class="item">
            +5</a></li>';
        }
        if ($paged + 1 <= $max_num_pages) {
            echo '<li class=""><a href="'.esc_url( get_pagenum_link( $paged + 1 ) ).'" class="item prev-next next">
            Next <i class="ion-ios-arrow-right"></i></i></a></li>';
        }
        if ($max_num_pages > 9) {
            echo '<li class=""><a href="'.esc_url( get_pagenum_link( $max_num_pages ) ).'" class="item">
            ...</a></li>';
        }
    }
    echo '</ul></div>';
}


// Ajax Delete_product_cart Product
add_action('wp_ajax_Delete_product_cart', 'Delete_product_cart');
add_action('wp_ajax_nopriv_Delete_product_cart', 'Delete_product_cart');
function Delete_product_cart() {
    $data_productid = $_POST['data_productid'];

    $data = array();

    if($data_productid > 0) {
        $cartId = WC()->cart->generate_cart_id( $data_productid );
        $cartItemKey = WC()->cart->find_product_in_cart( $cartId );
        WC()->cart->remove_cart_item( $cartItemKey );
    }

    $data['result'] = '2';

    echo json_encode($data);
    die();
}



// Ajax Chang_number_order_shop
add_action('wp_ajax_Chang_number_order_shop', 'Chang_number_order_shop');
add_action('wp_ajax_nopriv_Chang_number_order_shop', 'Chang_number_order_shop');
function Chang_number_order_shop() {
    $data_order = $_POST['data_order'];

    $data = array();

    if($data_order > 0) {
        update_option( 'woocommerce_catalog_columns', $data_order );
    }

    $data['result'] = '2';

    echo json_encode($data);
    die();
}







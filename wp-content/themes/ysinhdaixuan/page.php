<?php get_header(); ?>

<?php
	$page_id       = get_the_ID();
	$page_name     = get_the_title();
	$page_content  = get_the_content(); //woo phải dùng the_content()
?>

<?php get_template_part("resources/views/page-banner"); ?>

<div class="about-us-area pt-80 pb-80">
    <div class="container">
        <div class="<?php if( is_cart() || is_checkout() ) {} else { echo 'wp-editor-fix'; } ?>">
            <?php the_content(); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
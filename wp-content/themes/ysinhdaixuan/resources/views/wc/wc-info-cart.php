<?php
    //cart
    $info_cart     = WC()->cart->cart_contents;
    $cart_count    = WC()->cart->get_cart_contents_count();
    $cart_total    = WC()->cart->get_cart_total();
    $cart_page_url = wc_get_cart_url();

    //checkout
    $checkout_page_url = wc_get_checkout_url();
?>

<div class="header-cart middle-same">
    <button class="icon-cart">
        <i class="pe-7s-shopbag cart-bag"></i>
        <span class="d-md-inline-block d-none">
            <span class="count-amount"><?php echo $cart_total; ?></span>
            <i class="ion-chevron-down cart-down"></i>
            <span class="count-style"><?php if($cart_count > 0) { echo $cart_count; } else { echo 0; } ?></span>
        </span>
    </button>
    <div class="shopping-cart-content shopping-cart-green">
        <?php if($cart_count > 0) { ?>

            <ul>

                <?php
                    foreach ($info_cart as $info_cart_kq) {
                        
                    $product_id         = $info_cart_kq["product_id"];
                    $product_title      = get_the_title($product_id);
                    $product_link       = get_permalink($product_id);
                    $product_image      = getPostImage($product_id,"p-product");

                    $product_quantity   = $info_cart_kq["quantity"];
                    $product_total      = $info_cart_kq["line_total"];
                    $product_price      = format_price_donvi($product_total / $product_quantity);
                ?>
                    <li class="single-shopping-cart single-cart-item">
                        <div class="shopping-cart-img">
                            <a href="<?php echo $product_link; ?>">
                                <img alt="<?php echo $product_title; ?>" src="<?php echo $product_image; ?>">
                            </a>
                        </div>
                        <div class="shopping-cart-title">
                            <h4><a href="<?php echo $product_link; ?>"><?php echo $product_title; ?></a></h4>
                            <h6>SL: <?php echo $product_quantity; ?></h6>
                            <span><?php echo $product_price; ?></span>
                        </div>
                        <div class="shopping-cart-delete del-icon trash" data-productid="<?php echo $product_id; ?>">
                            <a href="javascript:void(0)">
                                <i class="ion-android-close"></i>
                            </a>
                        </div>
                    </li>
                <?php } ?>

            </ul>

            <div class="shopping-cart-total">
                <!-- <h4>Phí vận chuyển : <span>20.000</span></h4> -->
                <h4>Tổng tiền : <span class="shop-total"><?php echo $cart_total; ?></span></h4>
            </div>
            <div class="shopping-cart-btn">
                <a class="btn-style btn-hover" href="<?php echo $cart_page_url; ?>">Xem giỏ hàng</a>
                <a class="btn-style btn-hover" href="<?php echo $checkout_page_url; ?>">Thanh toán</a>
            </div>

        <?php } else { ?>
            <?php _e('Giỏ hàng trống !', 'text_domain'); ?>
        <?php } ?>
    </div>
</div>
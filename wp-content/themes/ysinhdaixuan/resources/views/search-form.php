<div class="header-search middle-same">

	<form class="header-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	    <input type="text" class="form-control" required="required" placeholder="<?php _e('Từ khóa tìm kiếm ...', 'text_domain'); ?>" name="s" value="<?php echo get_search_query(); ?>">
	    <button type="submit" class="">
	    	<i class="ion-ios-search-strong"></i>
	    </button>
	</form>
	
</div>
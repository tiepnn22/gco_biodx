<?php
	$post_id            = get_the_ID();
	$post_title 		= get_the_title($post_id);
	// $post_content 		= wpautop(get_the_content($post_id));
	$post_date 			= get_the_date('d/m/Y',$post_id);
	$post_link 			= get_permalink($post_id);
	$post_image 		= getPostImage($post_id,"p-product");
	$post_excerpt 		= cut_string(get_the_excerpt($post_id),200,'...');
	$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
	$post_tag 			= get_the_tags($post_id);

	$terms = wp_get_object_terms($post_id, 'product_cat');

    // woocommerce
    $product = new WC_product($post_id);
    // wc gallery
    $single_product_gallery = $product->get_gallery_image_ids();
?>

<div class="col-lg-12">
    <div class="product-list-wrapper shop-border mb-40 pb-40">
        <div class="product-img">
            <div class="product-img-slider">
                <a href="<?php echo $post_link; ?>">
                	<img src="<?php echo $post_image; ?>" alt="">
                </a>

                <?php if(!empty( $single_product_gallery )) { ?>
                <?php
                    foreach( $single_product_gallery as $single_product_gallery_kq ){

                    $post_image = wp_get_attachment_url( $single_product_gallery_kq );
                ?>
                    <a href="<?php echo $post_link; ?>">
                        <img src="<?php echo $post_image; ?>" alt="">
                    </a>
                <?php } ?>
                <?php } ?>
            </div>
        </div>

        <div class="product-list-content">

            <?php if(!empty( $terms )) { ?>
            <span class="cat-separator">
	            <?php
	                foreach ($terms as $foreach_kq) {

	                $term_id        = $foreach_kq->term_id;
	                $taxonomy_slug  = $foreach_kq->taxonomy;
	                $term_name      = get_term( $term_id, $taxonomy_slug )->name;

	                	echo $term_name.'<p>,</p> ';
	            	}
	            ?>
            </span>
            <?php } ?>

            <h4>
				<a title="<?php echo $post_title; ?>" href="<?php echo $post_link; ?>">
					<?php echo $post_title; ?>
				</a>
            </h4>

            <?php echo show_price_old_price($post_id); ?>

            <p><?php echo $post_excerpt; ?></p>

            <div class="product-action">
                <?php echo show_add_to_cart_button_ajax($post_id); ?>
            </div>
        </div>
    </div>
</div>
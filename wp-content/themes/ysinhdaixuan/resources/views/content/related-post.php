<?php
    $post_id            = get_the_ID();
    $post_title         = get_the_title($post_id);
    // $post_content       = wpautop(get_the_content($post_id));
    $post_date          = get_the_date('d',$post_id).' tháng '.get_the_date('m',$post_id).' năm '.get_the_date('Y',$post_id);
    $post_link          = get_permalink($post_id);
    $post_image         = getPostImage($post_id,"p-post");
    $post_excerpt       = cut_string(get_the_excerpt($post_id),200,'...');
    $post_author        = get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
    $post_tag           = get_the_tags($post_id);
    $post_comment       = wp_count_comments($post_id);
    $post_comment_total = $post_comment->total_comments;
?>

<div class="single-recent-post mb-20">
    <div class="recent-post-img">
        <a title="<?php echo $post_title; ?>" href="<?php echo $post_link; ?>">
            <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
        </a>
    </div>
    <div class="product-content">
        <h4>
            <a title="<?php echo $post_title; ?>" href="<?php echo $post_link; ?>">
                <?php echo $post_title; ?>
            </a>
        </h4>
        <span><?php echo $post_date; ?></span>
    </div>
</div>
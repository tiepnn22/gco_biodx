<?php
	$post_id            = get_the_ID();
	$post_title 		= get_the_title($post_id);
	// $post_content 		= wpautop(get_the_content($post_id));
	$post_date 			= get_the_date('d',$post_id).' tháng '.get_the_date('m',$post_id).' năm '.get_the_date('Y',$post_id);
	$post_link 			= get_permalink($post_id);
	$post_image 		= getPostImage($post_id,"p-post");
	$post_excerpt 		= cut_string(get_the_excerpt($post_id),200,'...');
	$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
	$post_tag 			= get_the_tags($post_id);

	$get_category = get_the_category($post_id);
?>

<div class="col-lg-4 col-md-6 col-12">
    <div class="blog-wrapper mb-30 main-blog">
        <div class="blog-img mb-20">
			<a title="<?php echo $post_title; ?>" href="<?php echo $post_link; ?>">
				<img src="<?php echo $post_image; ?>" class="img-fluid" width="100%" alt="<?php echo $post_title; ?>">
			</a>
        </div>

        <?php if(!empty( $get_category )) { ?>
        <span class="cat-separator">
            <?php
                foreach ($get_category as $foreach_kq) {

                $term_id        = $foreach_kq->term_id;
                $taxonomy_slug  = $foreach_kq->taxonomy;
                $term_name      = get_term( $term_id, $taxonomy_slug )->name;

                	echo $term_name.'<p>,</p> ';
            	}
            ?>
        </span>
        <?php } ?>

        <h3>
			<a title="<?php echo $post_title; ?>" href="<?php echo $post_link; ?>">
				<?php echo $post_title; ?>
			</a>
        </h3>

        <p><?php echo $post_excerpt; ?></p>

        <div class="blog-meta-bundle">
            <div class="blog-meta">
                <ul>
                    <li><?php echo $post_date; ?></li>
                </ul>
            </div>
            <div class="blog-readmore">
                <a title="<?php echo $post_title; ?>" href="<?php echo $post_link; ?>">
                	Đọc thêm <i class="fa fa-angle-double-right"></i>
                </a>
            </div>
        </div>
    </div>
</div>
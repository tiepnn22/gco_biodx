<?php
    $next_post     = get_next_post();
	$previous_post = get_previous_post();
?>

<div class="pro-dec-btn">

	<?php
		if( !empty($previous_post) ) {
			$post_id    = $previous_post->ID;
			$post_title = get_the_title($post_id);
			$post_link  = get_permalink($post_id);
	?>
        <a href="<?php echo $post_link; ?>"><i class="fa fa-angle-left"></i></a>
    <?php } ?>

	<?php
		if( !empty($next_post) ) {
			$post_id    = $next_post->ID;
			$post_title = get_the_title($post_id);
			$post_link  = get_permalink($post_id);
	?>
        <a href="<?php echo $post_link; ?>"><i class="fa fa-angle-right"></i></a>
    <?php } ?>

</div>
<?php
	global $post;
	$terms 		= get_the_terms( $post->ID , 'product_cat', 'string');
	$term_ids 	= wp_list_pluck($terms,'term_id');
	
	$query = new WP_Query( array(
		'post_type' 	 => 'product',
		'tax_query' 	 => array(
			array(
				'taxonomy' 	=> 'product_cat',
				'field' 	=> 'id',
				'terms' 	=> $term_ids,
				'operator'	=> 'IN'
			 )),
		'posts_per_page' => -1,
		'orderby' 		 => 'date',
		'post__not_in'	 => array($post->ID)
	) );
?>


<div class="product-area pb-45">
    <div class="container">
        <div class="section-border mb-10">
            <h4 class="section-title section-bg-white">Sản phẩm liên quan</h4>
        </div>
        <div class="product-slider-nav nav-style"></div>
        <div class="headphone-slider-active product-slider owl-carousel nav-style">

			<?php
				if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
			?>

				<?php get_template_part('resources/views/content/related-product', get_post_format()); ?>

			<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

        </div>
    </div>
</div>

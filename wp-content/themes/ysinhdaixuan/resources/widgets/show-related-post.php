<?php
class show_related_post extends WP_Widget {
    function __construct() {
        parent::__construct(
            'show_related_post',
            'Core - Hiển thị bài viết liên quan',
            array( 'description'  =>  'Hiển thị bài viết liên quan' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => 'Hiển thị bài viết liên quan',
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);

        echo '<p>';
            echo 'Tiêu đề :';
            echo '<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/>';
        echo '</p>';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );

        global $post;
        $categories = get_the_category($post->ID);

        $category_ids = array();
        foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
        $args=array(
            'category__in'         => $category_ids,
            'post__not_in'         => array($post->ID),
            'posts_per_page'       => 3,
            'ignore_sticky_posts'  => 1
        );
        $query = new wp_query( $args );

        echo $before_widget; ?>
        <h4 class="shop-sidebar-title"><?php echo $title; ?></h4>
        <div class="recent-post-wrapper mt-20">

            <?php
                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
            ?>

                <?php get_template_part('resources/views/content/related-post', get_post_format()); ?>

            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

        </div>
        <?php echo $after_widget;
    }
}
function create_showrelatedpost_widget() {
    register_widget('show_related_post');
}
add_action( 'widgets_init', 'create_showrelatedpost_widget' );
?>
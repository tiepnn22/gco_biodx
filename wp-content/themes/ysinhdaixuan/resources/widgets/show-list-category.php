<?php
class show_list_category extends WP_Widget {
    function __construct() {
        parent::__construct(
            'show_list_category',
            'Core - Hiển thị danh sách category',
            array( 'description'  =>  'Hiển thị danh sách category' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => 'Hiển thị danh sách category',
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);

        echo '<p>';
            echo 'Tiêu đề :';
            echo '<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/>';
        echo '</p>';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $categories = get_categories( array(
            'orderby' => 'name',
            'parent'  => 0
        ) );

        echo $before_widget; ?>
        <h4 class="shop-sidebar-title"><?php echo $title; ?></h4>
        <div class="shop-catigory">
            <ul id="faq" class="news-categories">

                <?php
                    $i=1;
                    foreach ( $categories as $category ) {
                        $category_id = $category->term_id;
                        $category_name = $category->name;

                        $child_cats = get_term_children( $category_id, 'category' );
                        $count = count($child_cats);
                ?>
                        <li>
                            <a href="#">
                                <?php echo $category_name; ?>
                            </a>

                            <?php if($count > 0) { ?>
                            <ul>

                                <?php
                                    foreach ($child_cats as $childs) {
                                        $child_cat      = get_term_by( 'id', $childs, 'category' );

                                        $child_cat_id   = $child_cat->term_id;
                                        $child_cat_name = $child_cat->name;
                                        $child_cat_link = esc_url(get_term_link($child_cat_id));
                                ?>
                                        <li>
                                            <a href="<?php echo $child_cat_link; ?>"><?php echo $child_cat_name; ?></a>
                                        </li>
                                <?php } ?>

                            </ul>
                            <?php } ?>

                        </li>
                <?php
                    $i++; }
                ?>

            </ul>
        </div>
        <?php echo $after_widget;
    }
}
function create_showlistcategory_widget() {
    register_widget('show_list_category');
}
add_action( 'widgets_init', 'create_showlistcategory_widget' );
?>
<?php
class show_list_tag extends WP_Widget {
    function __construct() {
        parent::__construct(
            'show_list_tag',
            'Core - Hiển thị danh sách tag',
            array( 'description'  =>  'Hiển thị danh sách tag' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => 'Hiển thị danh sách tag',
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);

        echo '<p>';
            echo 'Tiêu đề :';
            echo '<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/>';
        echo '</p>';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $tags = get_tags( array(
            'orderby' => 'name',
            'parent'  => 0
        ) );

        echo $before_widget; ?>
        <h4 class="shop-sidebar-title"><?php echo $title; ?></h4>
        <div class="shop-tags mt-25">
            <ul>

                <?php
                    foreach ( $tags as $foreach_kq ) {
                  
                    $post_id = $foreach_kq->term_id;
                    $post_title = $foreach_kq->name;
                    $post_link = get_term_link($post_id);
                ?>
                    <li>
                        <a href="<?php echo $post_link; ?>">
                            <?php echo $post_title; ?>
                        </a>
                    </li>
                <?php } ?>

            </ul>
        </div>
        <?php echo $after_widget;
    }
}
function create_showlisttag_widget() {
    register_widget('show_list_tag');
}
add_action( 'widgets_init', 'create_showlisttag_widget' );
?>
<?php get_header(); ?>

<?php
	$s = $_GET['s'];
?>

<?php get_template_part("resources/views/page-banner"); ?>

<div class="shop-page-area pt-30 pb-65">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <div class="tab-content jump">
                    <div class="tab-pane pb-20 active" id="product-grid">
                        <div class="row">

                            <?php
                                $query = query_search_post_paged($s, array('product'), 4);

                                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                                $max_num_pages      = $query->max_num_pages;
                                $total_post         = $query->found_posts;
                                $total_post_start   = ($paged -1) * 4 + 1;
                                $total_post_end     = min( $total_post, $paged * 4 );

                                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                            ?>
                                <?php get_template_part('resources/views/content/category-product-grid', get_post_format()); ?>

                            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                        </div>
                    </div>

                    <!--pagination-->
                    <div class="row">
                        <div class="col-12">
                            <div class="pagination-total-pages">
                                <?php echo paginationCustom( $max_num_pages ); ?>
                                <div class="total-pages">
                                    <p>Hiển thị <?php echo $total_post_start; ?> - <?php echo $total_post_end; ?> / <?php echo $total_post; ?> kết quả </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
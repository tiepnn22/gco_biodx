<?php
    //field
    $customer_slogan    = get_option('blogdescription');
    $customer_address   = get_field('customer_address', 'option');
    $customer_phone     = get_field('customer_phone', 'option');
    $customer_email     = get_field('customer_email', 'option');

    $f_brand    = get_field('f_brand', 'option');
    $f_service  = get_field('f_service', 'option');

    $f_form_title   = get_field('f_form_title', 'option');
    $f_form_desc    = get_field('f_form_desc', 'option');
    $f_form_id      = get_field('f_form', 'option');
    $f_form         = do_shortcode('[contact-form-7 id="'.$f_form_id.'"]');

    $f_socical_facebook     = get_field('f_socical_facebook', 'option');
    $f_socical_twitter      = get_field('f_socical_twitter', 'option');
    $f_socical_insta        = get_field('f_socical_insta', 'option');
    $f_socical_googleplus   = get_field('f_socical_googleplus', 'option');
    $f_socical_rss          = get_field('f_socical_rss', 'option');
    $f_socical_dribbble     = get_field('f_socical_dribbble', 'option');
    $f_socical_vimeo        = get_field('f_socical_vimeo', 'option');
    $f_socical_pinterest    = get_field('f_socical_pinterest', 'option');
    $f_socical_skype        = get_field('f_socical_skype', 'option');

    $f_bottom_copyright = get_field('f_bottom_copyright', 'option');
?>

<?php if(!empty( $f_brand )) { ?>
<div class="brand-logo-area">
    <div class="container">
        <div class="brand-logo-active owl-carousel border-top-2 ptb-60">

            <?php
                foreach ($f_brand as $foreach_kq) {

                $post_image = $foreach_kq;
            ?>
                <div class="single-brand-logo">
                    <img alt="" src="<?php echo $post_image; ?>">
                </div>
            <?php } ?>

        </div>
    </div>
</div>
<?php } ?>

<footer class="footer-area gray-bg-2 pt-65">

    <?php if(!empty( $f_service )) { ?>
    <div class="container">
        <div class="shop-service-wrapper service-green white-bg">
            <div class="row">

                <?php
                    foreach ($f_service as $foreach_kq) {

                    $post_title = $foreach_kq["title"];
                    $post_desc  = $foreach_kq["desc"];
                ?>
                    <div class="col-lg-4 col-md-4">
                        <div class="shop-service-content text-center service-black-color">
                            <h4 class="text-capitalize"><?php echo $post_title; ?></h4>
                            <p><?php echo $post_desc; ?></p>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
    <?php } ?>

    <div class="container pt-60 mb-15">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="footer-widget footer-widget-green mb-40 footer-black-color">
                    <div class="footer-title mb-30">
                        <h4 class="text-capitalize">Giới thiệu</h4>
                    </div>
                    <div class="footer-about">
                        <p><?php echo $customer_slogan; ?></p>
                        <div class="footer-contact mt-20">
                            <ul>
                                <li>Địa chỉ: <?php echo $customer_address; ?></li>
                                <li>Điện thoại: <a href="tel:<?php echo str_replace(' ','',$customer_phone);?>" title="">
                                    <?php echo $customer_phone; ?></a>
                                </li>
                                <li>Email: <a href="mailto:<?php echo $customer_email; ?>"><?php echo $customer_email; ?></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="social-icon mr-40">
                        <ul>
                            <li><a class="facebook" href="<?php echo $f_socical_facebook; ?>" target="_blank">
                                <i class="ion-social-facebook"></i></a></li>
                            <li><a class="twitter" href="<?php echo $f_socical_twitter; ?>" target="_blank">
                                <i class="ion-social-twitter"></i></a></li>
                            <li><a class="instagram" href="<?php echo $f_socical_insta; ?>" target="_blank">
                                <i class="ion-social-instagram-outline"></i></a></li>
                            <li><a class="googleplus" href="<?php echo $f_socical_googleplus; ?>" target="_blank">
                                <i class="ion-social-googleplus-outline"></i></a></li>
                            <li><a class="rss" href="<?php echo $f_socical_rss; ?>" target="_blank">
                                <i class="ion-social-rss"></i></a></li>
                            <li><a class="dribbble" href="<?php echo $f_socical_dribbble; ?>" target="_blank">
                                <i class="ion-social-dribbble-outline"></i></a></li>
                            <li><a class="vimeo" href="<?php echo $f_socical_vimeo; ?>" target="_blank">
                                <i class="ion-social-vimeo"></i></a></li>
                            <li><a class="pinterest" href="<?php echo $f_socical_pinterest; ?>" target="_blank">
                                <i class="ion-social-pinterest"></i></a></li>
                            <li><a class="skype" href="<?php echo $f_socical_skype; ?>" target="_blank">
                                <i class="ion-social-skype-outline"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-6">
                <div class="footer-widget footer-widget-green mb-40 footer-black-color">
                    <div class="footer-title mb-30">
                        <h4 class="text-capitalize"><?php echo wp_get_nav_menu_name("f_benefit" ); ?></h4>
                    </div>
                    <?php
                        if(function_exists('wp_nav_menu')){
                            $args = array(
                                'theme_location'    =>  'f_benefit',
                                'container'         =>  'div',
                                'container_class'   =>  'footer-content',
                                'container_id'      =>  '',
                                'menu_class'        =>  '',
                            );
                            wp_nav_menu( $args );
                        }
                    ?>
                </div>
            </div>
            <div class="col-lg-2 col-md-6">
                <div class="footer-widget footer-widget-green mb-40 footer-black-color">
                    <div class="footer-title mb-30">
                        <h4 class="text-capitalize"><?php echo wp_get_nav_menu_name("f_cat" ); ?></h4>
                    </div>
                    <?php
                        if(function_exists('wp_nav_menu')){
                            $args = array(
                                'theme_location'    =>  'f_cat',
                                'container'         =>  'div',
                                'container_class'   =>  'footer-content',
                                'container_id'      =>  '',
                                'menu_class'        =>  '',
                            );
                            wp_nav_menu( $args );
                        }
                    ?>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="footer-widget footer-widget-green mb-40 footer-black-color">
                    <div class="footer-title mb-30">
                        <h4 class="text-capitalize"><?php echo $f_form_title; ?></h4>
                    </div>
                    <div class="footer-newsletter">
                        <p><?php echo $f_form_desc; ?></p>
                        <div id="mc_embed_signup" class="subscribe-form-2">
                            <?php if(!empty( $f_form )) { ?>
                                <?php echo $f_form; ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom pb-25 pt-25 white-bg green-color">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="copyright text-center footer-black-color">
                        <p><?php echo $f_bottom_copyright; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php get_template_part("resources/views/socical-footer"); ?>

<?php wp_footer(); ?>
</body>
</html>
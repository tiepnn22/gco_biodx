<?php get_header(); ?>

<?php
	$post_id = get_the_ID();

	$get_category = get_the_category($post_id);
	foreach ( $get_category as $get_category_kq ) {
	    $cat_id   = $get_category_kq->term_id;
	}
	$cat_name = get_cat_name($cat_id);
	
	//info post
	$single_post_title 		= get_the_title($post_id);
	$single_post_date 		= get_the_date('d',$post_id).' tháng '.get_the_date('m',$post_id).' năm '.get_the_date('Y',$post_id);
	$single_post_link 		= get_permalink($post_id);
    $single_post_image 		= getPostImage($post_id,"full");
	$single_post_excerpt 	= cut_string(get_the_excerpt($post_id),300,'...');
	$single_recent_author 	= get_user_by( 'ID', get_post_field( 'post_author', get_the_author() ) );
	$single_post_author 	= $single_recent_author->display_name;
    $single_post_tag 		= get_the_tags($post_id);
?>

<?php get_template_part("resources/views/page-banner"); ?>

<div class="blog-area pt-65 pb-65">
    <div class="container">
        <div class="row">

            <?php get_sidebar(); ?>

            <div class="col-xl-9 col-lg-8">
                <div class="blog-details-wrapper">
                    <div class="single-blog-wrapper">
                        <div class="blog-img mb-30">
                            <img src="<?php echo $single_post_image; ?>" alt="<?php echo $single_post_title; ?>">
                        </div>
                        <div class="blog-details-content">
                            <h1><?php echo $single_post_title; ?></h1>
                            <div class="blog-meta mb-20">
                                <ul>
                                    <li><?php echo $single_post_date; ?></li>
                                </ul>
                            </div>
                        </div>
                        
                        <div class="wp-editor-fix">
                            <?php the_content(); ?>
                        </div>

                        <div class="blog-dec-tags-social">
                            <div class="blog-dec-tags">
                                <ul>
					                <?php
					                	if( !empty($single_post_tag) ) {
					                    foreach ($single_post_tag as $single_post_tag_kq) {

					                    $post_id = $single_post_tag_kq->term_id;
					                    $post_title = $single_post_tag_kq->name;
					                    $post_link = get_term_link($post_id);
					                ?>
					                	<li><a href="<?php echo $post_link; ?>"><?php echo $post_title; ?></a></li>
		                            <?php } } ?>
                                </ul>
                            </div>

                            <div class="blog-dec-social">
                                <span>Chia sẻ:</span>
                                <?php get_template_part("resources/views/socical-bar"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="blog-comment-wrapper mt-55">
                        <h4 class="blog-dec-title">Bình luận</h4>
                        <div class="single-comment-wrapper mt-50">
							<!--fb comments-->
							<div class="fb-comments" data-href='<?php the_permalink();?>' data-width="100%" data-numposts="5" data-colorscheme="light"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
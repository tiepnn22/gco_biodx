<?php get_header(); ?>

<?php
	$category_info 	= get_category_by_slug( get_query_var( 'category_name' ) );
	$cat_id 		= $category_info->term_id;
	$cat_name 		= get_cat_name($cat_id);
	$cat_excerpt 	= wpautop(category_description($cat_id));
	$cat_link 		= esc_url(get_term_link($cat_id));
?>

<?php get_template_part("resources/views/page-banner"); ?>

<div class="blog-area pt-65 pb-65">
    <div class="container">
        <div class="row">

            <?php
            	$query = query_post_by_category_paged($cat_id, 9);
            	
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $max_num_pages      = $query->max_num_pages;
                $total_post         = $query->found_posts;
                $total_post_start   = ($paged -1) * 9 + 1;
                $total_post_end     = min( $total_post, $paged * 9 );

                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
            ?>

                <?php get_template_part('resources/views/content/category-post', get_post_format()); ?>

            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

        </div>

        <div class="row">
            <div class="col-12">
                <div class="pagination-total-pages">
                    <?php echo paginationCustom( $max_num_pages ); ?>
                    <div class="total-pages">
                        <p>Hiển thị <?php echo $total_post_start; ?> - <?php echo $total_post_end; ?> / <?php echo $total_post; ?> kết quả </p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<?php get_footer(); ?>
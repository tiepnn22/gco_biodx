<?php
	/*
	Template Name: Mẫu Tin tức
	*/
?>

<?php get_header(); ?>

<?php
    $page_id        = get_the_ID();
    $page_name      = get_the_title();
    $page_content   = wpautop(get_the_content());
?>

<?php get_template_part("resources/views/page-banner"); ?>

<div class="blog-area pt-65 pb-65">
    <div class="container">
        <div class="row">

            <?php
                $query = query_post_by_custompost_paged('post', 9);
                
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $max_num_pages      = $query->max_num_pages;
                $total_post         = $query->found_posts;
                $total_post_start   = ($paged -1) * 9 + 1;
                $total_post_end     = min( $total_post, $paged * 9 );

                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
            ?>

                <?php get_template_part('resources/views/content/category-post', get_post_format()); ?>

            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

        </div>

        <div class="row">
            <div class="col-12">
                <div class="pagination-total-pages">
                    <?php echo paginationCustom( $max_num_pages ); ?>
                    <div class="total-pages">
                        <p>Hiển thị <?php echo $total_post_start; ?> - <?php echo $total_post_end; ?> / <?php echo $total_post; ?> kết quả </p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<?php get_footer(); ?>
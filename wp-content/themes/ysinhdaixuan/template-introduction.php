<?php
	/*
	Template Name: Mẫu Giới thiệu
	*/
?>

<?php get_header(); ?>

<?php
    $page_id        = get_the_ID();
    $page_name      = get_the_title();
    $page_content   = wpautop(get_the_content());

    //field
    $intro_intro_image  = get_field('intro_intro_image');
    $intro_intro_title  = get_field('intro_intro_title');
    $intro_intro_desc   = get_field('intro_intro_desc');
    $intro_intro_button = get_field('intro_intro_button');
    $intro_intro_url    = get_field('intro_intro_url');

    $intro_achievement_content = get_field('intro_achievement_content');

    $intro_team_title   = get_field('intro_team_title');
    $intro_team_content = get_field('intro_team_content');

    $intro_image_ads_one        = get_field('intro_image_ads_one');
    $intro_image_ads_one_url    = get_field('intro_image_ads_one_url');
    $intro_image_ads_two        = get_field('intro_image_ads_two');
    $intro_image_ads_two_url    = get_field('intro_image_ads_two_url');

    $intro_testimonial_title    = get_field('intro_testimonial_title');
    $intro_testimonial_content  = get_field('intro_testimonial_content');
?>

<?php get_template_part("resources/views/page-banner"); ?>

<div class="about-us-area pt-80 pb-80">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-7 d-flex align-items-center">
                <div class="overview-content-2">
                    <h2 class="text-uppercase"><?php echo $intro_intro_title; ?></h2>
                    <p><?php echo $intro_intro_desc; ?></p>
                    <div class="overview-btn mt-45">
                        <a class="btn-style-2" href="<?php echo $intro_intro_url; ?>"><?php echo $intro_intro_button; ?></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-5">
                <div class="overview-img text-center">
                    <a href="<?php echo $intro_intro_url; ?>">
                        <img src="<?php echo $intro_intro_image; ?>" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if(!empty( $intro_achievement_content )) { ?>
<div class="project-count-area gray-bg pt-100 pb-70">
    <div class="container">
        <div class="row">

            <?php
                foreach ($intro_achievement_content as $foreach_kq) {

                $post_image  = $foreach_kq["image"];
                $post_title  = $foreach_kq["title"];
                $post_number = $foreach_kq["number"];
            ?>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-count text-center mb-30">
                        <div class="count-icon">
                            <img src="<?php echo $post_image; ?>" alt="">
                        </div>
                        <div class="count-title">
                            <h2 class="count"><?php echo $post_number; ?></h2>
                            <span class="text-capitalize"><?php echo $post_title; ?></span>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
    </div>
</div>
<?php } ?>

<div class="team-area pt-75 pb-50">
    <div class="container">
        <div class="text-center mb-40">
            <h4 class="text-capitalize section-title-about"><?php echo $intro_team_title; ?></h4>
        </div>

        <?php if(!empty( $intro_team_content )) { ?>
        <div class="row">

            <?php
                foreach ($intro_team_content as $foreach_kq) {

                $post_image     = $foreach_kq["image"];
                $post_title     = $foreach_kq["title"];
                $post_job       = $foreach_kq["job"];
                $post_facebook  = $foreach_kq["facebook"];
                $post_twitter   = $foreach_kq["twitter"];
                $post_instagram = $foreach_kq["instagram"];
            ?>
                <div class="col-lg-3 col-md-6">
                    <div class="team-wrapper mb-30">
                        <div class="team-img">
                            <a href="javascript:void(0)">
                                <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
                            </a>
                            <div class="team-action">
                                <a class="action-plus facebook" href="<?php echo $post_facebook; ?>">
                                    <i class="ion-social-facebook"></i>
                                </a>
                                <a class="action-heart twitter" href="<?php echo $post_twitter; ?>">
                                    <i class="ion-social-twitter"></i>
                                </a>
                                <a class="action-cart instagram" href="<?php echo $post_instagram; ?>">
                                    <i class="ion-social-instagram"></i>
                                </a>
                            </div>
                        </div>
                        <div class="team-content text-center">
                            <h4><?php echo $post_title; ?></h4>
                            <span><?php echo $post_job; ?></span>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
        <?php } ?>

    </div>
</div>

<div class="banner-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8">
                <div class="banner-img banner-hover banner-mrg">
                    <a href="<?php echo $intro_image_ads_one_url; ?>">
                        <img src="<?php echo $intro_image_ads_one; ?>" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="banner-img banner-mrg-left-2 banner-hover">
                    <a href="<?php echo $intro_image_ads_two_url; ?>">
                        <img src="<?php echo $intro_image_ads_two; ?>" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="testimonials-area bg-img pt-75 pb-80">
    <div class="container">
        <div class="text-center mb-40">
            <h4 class="text-capitalize section-title-about"><?php echo $intro_testimonial_title; ?></h4>
        </div>

        <?php if(!empty( $intro_testimonial_content )) { ?>
        <div class="row">
            <div class="col-lg-12 col-md-12 d-flex align-items-center">
                <div class="testimonial-active about-testi-active owl-carousel">

                    <?php
                        foreach ($intro_testimonial_content as $foreach_kq) {

                        $post_title     = $foreach_kq["title"];
                        $post_job       = $foreach_kq["job"];
                        $post_desc      = $foreach_kq["desc"];
                        $post_review    = $foreach_kq["review"][0];
                    ?>
                        <div class="single-testimonial about-testi text-center">
                            <div class="quote-icon">
                                <i class="fa fa-quote-left"></i>
                            </div>
                            <p><?php echo $post_desc; ?></p>
                            <div class="testimonial-rating">

                                <?php
                                    for ($i=1; $i <= $post_review; $i++) { 
                                        echo '<i class="ion-android-star-outline theme-star-coffee"></i>';
                                    }
                                ?>

                                <!-- <i class="ion-android-star-outline theme-star-coffee"></i> -->
                                <!-- <i class="ion-android-star-outline"></i> -->
                            </div>
                            <h4><?php echo $post_title; ?> <span> / <?php echo $post_job; ?></span></h4>
                        </div>
                    <?php } ?>

                </div>
            </div>
        </div>
        <?php } ?>

    </div>
</div>

<?php get_footer(); ?>
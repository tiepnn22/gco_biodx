<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<?php
    $product_id = get_the_ID();

	$terms = wp_get_object_terms($post->ID, 'product_cat');
	if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0])) $term = $terms[0];
    $term_id        = $term->term_id;
    $term_name      = $term->name;
    // $term_excerpt   = wpautop($term->description);
    // $term_link      = esc_url(get_term_link($term_id));
    $taxonomy_slug  = $term->taxonomy;
    // $thumbnail_id        = get_term_meta( $term_id, 'thumbnail_id', true ); // woo
    // $term_image_check    = wp_get_attachment_url( $thumbnail_id ); // woo
    // $term_image          = (!empty($term_image_check)) ? $term_image_check : ''; // woo

    //woocommerce
    $product = new WC_product($product_id);

        // tag
        $single_product_tag = $product->get_tags();

        //gallery
        $single_product_gallery = $product->get_gallery_image_ids();

        // check stock
        if ( ! $product->managing_stock() && ! $product->is_in_stock() ) {
            $product_stock = 'Hết hàng';
        } else {
            $product_stock = 'Còn hàng';
        }

    //info product
    $single_product_title       = get_the_title($product_id);
    $single_product_date        = get_the_date('d/m/Y', $product_id);
    $single_product_link        = get_permalink($product_id);
    $single_product_image       = getPostImage($product_id,"full");
    $single_product_excerpt     = get_the_excerpt($product_id);
    $single_recent_author       = get_user_by( 'ID', get_post_field( 'post_author', get_the_author() ) );
    $single_product_author      = $single_recent_author->display_name;
    // $single_product_tag         = get_the_tags($product_id);
?>

<div class="product-details pt-65 pb-65">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="product-details-img">
                    <img class="zoompro" src="<?php echo $single_product_image; ?>" data-zoom-image="<?php echo $single_product_image; ?>" alt="zoom" />

                    <div id="gallery" class="mt-20 product-dec-slider owl-carousel">
                        <a data-image="<?php echo $single_product_image; ?>" data-zoom-image="<?php echo $single_product_image; ?>">
                            <img src="<?php echo $single_product_image; ?>" alt="">
                        </a>
                        
                        <?php if(!empty( $single_product_gallery )) { ?>
                        <?php
                            foreach( $single_product_gallery as $foreach_kq ){

                            $post_image = wp_get_attachment_url( $foreach_kq );
                        ?>
                            <a data-image="<?php echo $post_image; ?>" data-zoom-image="<?php echo $post_image; ?>">
                                <img src="<?php echo $post_image; ?>" alt="">
                            </a>
                        <?php } ?>
                        <?php } ?>
                    </div>

                    <?php echo show_sale($product_id); ?>
                </div>
            </div>

            <div class="col-lg-6 col-md-12">
                <div class="product-details-content">
                    <h1><?php echo $single_product_title; ?></h1>
                    <?php echo show_price_old_price($product_id); ?>
                    <div class="in-stock">
                        <p>Tình trạng: <span><?php echo $product_stock; ?></span></p>
                    </div>
                    <p><?php echo $single_product_excerpt; ?></p>
                    <div class="quality-add-to-cart">
                        <label>SL:</label>
                        <?php echo show_add_to_cart_button_quantity($product_id); ?>
                    </div>

                    <div class="pro-dec-categories">
                        <ul>
                            <li class="categories-title">Danh mục:</li>

                            <?php if(!empty( $terms )) { ?>
                            <?php
                                foreach ($terms as $foreach_kq) {

                                $term_id        = $foreach_kq->term_id;
                                $taxonomy_slug  = $foreach_kq->taxonomy;
                                $term_name      = get_term( $term_id, $taxonomy_slug )->name;
                            ?>
                                    <li><a href="#"><?php echo $term_name.'<span>,</span> '; ?></a></li>
                            <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>

                    <div class="pro-dec-categories">
                        <ul>
                            <li class="categories-title">Tags: </li>

                            <?php if(!empty( $single_product_tag )) { ?>
                            <?php echo $single_product_tag; ?>
                            <?php } ?>
                        </ul>
                    </div>

                    <div class="pro-dec-social">
                        <?php get_template_part("resources/views/socical-bar"); ?>
                    </div>

                    <?php get_template_part("resources/views/previous-post-next-post"); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="description-review-area pt-25 pb-70">
    <div class="container">
        <div class="description-review-wrapper">
            <div class="description-review-topbar nav text-center">
                <a class="active" data-toggle="tab" href="#des-details1">Mô tả</a>
                <a data-toggle="tab" href="#des-details2">Tags</a>
                <a data-toggle="tab" href="#des-details3">Bình luận</a>
            </div>
            <div class="tab-content description-review-bottom">
                <div id="des-details1" class="tab-pane active">
                    <div class="product-description-wrapper wp-editor-fix">
                        <?php the_content(); ?>
                    </div>
                </div>
                <div id="des-details2" class="tab-pane">
                    <div class="product-anotherinfo-wrapper">
                        <ul>
                            <li><span>Tags:</span></li>

                            <?php if(!empty( $single_product_tag )) { ?>
                            <?php echo $single_product_tag; ?>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <div id="des-details3" class="tab-pane">
                    <div class="ratting-form-wrapper">
                        <h3>Bình luận:</h3>
                        <div class="ratting-form">
                            <!--fb comments-->
                            <div class="fb-comments" data-href='<?php the_permalink();?>' data-width="100%" data-numposts="5" data-colorscheme="light"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_template_part("resources/views/template-related-product"); ?>

<?php
get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */

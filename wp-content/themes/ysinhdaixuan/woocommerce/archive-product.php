<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' ); ?>

<?php
    $term_info         = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    $term_id           = $term_info->term_id;
    $term_name_check   = $term_info->name;
    $term_name         = (!empty($term_name_check)) ? $term_name_check : 'Sản phẩm';
    // $term_excerpt   = wpautop($term_info->description);
    // $term_link      = esc_url(get_term_link($term_id));
    // $taxonomy_name  = $term_info->taxonomy;
    $thumbnail_id   = get_term_meta( $term_id, 'thumbnail_id', true );
    $term_image     = wp_get_attachment_url( $thumbnail_id );
?>

<div class="shop-page-area pt-30 pb-65">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="shop-sidebar-wrapper gray-bg-7">

                    <?php dynamic_sidebar( 'sidebar-product' ); ?>

                </div>
            </div>

            <div class="col-lg-9">

                <?php if(!empty( $term_image )) { ?>
                <div class="banner-area pb-30 mrg-top-sm mrg-top-md">
                    <a href="javascript:void(0)" title="">
                        <img src="<?php echo $term_image; ?>" alt="<?php echo $term_name; ?>">
                    </a>
                </div>
                <?php } ?>

                <?php
                    if ( woocommerce_product_loop() ) {
                ?>

                    <div class="shop-topbar-wrapper">
                        <div class="shop-topbar-left">
                            <div class="grid-list-options nav view-mode">
                                <a href="#product-grid" data-view="product-grid" data-toggle="tab"><i class="fa fa-th"></i></a>
                                <a class="active" href="#product-list" data-view="product-list" data-toggle="tab"><i class="fa fa-list-ul"></i></a>
                            </div>
                            <?php
                                global $wp_query;
                                $total_all_page = wc_get_loop_prop( 'total' ) ? wc_get_loop_prop( 'total' ) : $wp_query->post_count;
                                $total_one_page = wc_get_default_products_per_row();

                                $current = isset( $current ) ? $current : wc_get_loop_prop( 'current_page' );

                                $first = ( $total_one_page * $current ) - $total_one_page + 1;
                                $last  = min( $total_all_page, $total_one_page * $current );
                            ?>
                            <p>
                                Hiển thị <?php echo $first; ?> - <?php echo $last; ?> / <?php echo $total_all_page; ?> kết quả
                            </p>
                        </div>
                        <div class="product-sorting-wrapper">
                            <div class="product-shorting shorting-style">
                                <label>Hiển thị:</label>
                                <select id="chang_number_order_shop">
                                    <option value="">Chọn</option>
                                    <option <?php if($total_one_page == 4) { echo 'selected'; } ?> value="4">4</option>
                                    <option <?php if($total_one_page == 8) { echo 'selected'; } ?> value="8">8</option>
                                    <option <?php if($total_one_page == 12) { echo 'selected'; } ?> value="12">12</option>
                                </select>
                            </div>
                           
                            <!--order-->
                            <?php do_action( 'woocommerce_before_shop_loop' ); ?>
                        </div>
                    </div>

                    <div class="tab-content jump">
                        <div class="tab-pane pb-20" id="product-grid">
                            <div class="row">

                                <?php
                                    // woocommerce_product_loop_start();
                                    if ( wc_get_loop_prop( 'total' ) ) {
                                        while ( have_posts() ) {
                                            the_post();

                                            // do_action( 'woocommerce_shop_loop' );
                                            // wc_get_template_part( 'content', 'product' );
                                            get_template_part('resources/views/content/category-product-grid', get_post_format());
                                        }
                                    }
                                    // woocommerce_product_loop_end();
                                ?>

                            </div>
                        </div>
                        <div class="tab-pane active" id="product-list">
                            <div class="row">

                                <?php
                                    // woocommerce_product_loop_start();
                                    if ( wc_get_loop_prop( 'total' ) ) {
                                        while ( have_posts() ) {
                                            the_post();

                                            // do_action( 'woocommerce_shop_loop' );
                                            // wc_get_template_part( 'content', 'product' );
                                            get_template_part('resources/views/content/category-product-list', get_post_format());
                                        }
                                    }
                                    // woocommerce_product_loop_end();
                                ?>

                            </div>
                        </div>

                        <!--pagination-->
                        <?php do_action( 'woocommerce_after_shop_loop' ); ?>
                    </div>

                <?php
                        // do_action( 'woocommerce_after_shop_loop' );
                    } else {
                        do_action( 'woocommerce_no_products_found' );
                    }
                ?>

            </div>
        </div>
    </div>
</div>

<?php

get_footer( 'shop' );

<?php get_header(); ?>

<h1 style="display: none;"><?php echo get_option('blogname'); ?> - <?php echo get_option('blogdescription'); ?></h1>

<?php
    //field
    $home_slide = get_field('home_slide');

    $home_ads_one = get_field('home_ads_one');

    $home_tab_product = get_field('home_tab_product');

    $home_product_selling_title			= get_field('home_product_selling_title');
    $home_product_selling_select_cat	= get_field('home_product_selling_select_cat');

    $home_ads_two = get_field('home_ads_two');

    $home_news_title = get_field('home_news_title');
    $home_news_select_post = get_field('home_news_select_post');
?>

<?php if(!empty( $home_slide )) { ?>
<div class="slider-area medical-slider">
    <div class="slider-active owl-dot-style-2 owl-dot-green owl-dot-4 owl-carousel">

        <?php
            foreach ($home_slide as $foreach_kq) {

            $post_image 	= $foreach_kq["image"];
            $post_node 		= $foreach_kq["node"];
            $post_title 	= $foreach_kq["title"];
            $post_desc 		= $foreach_kq["desc"];
            $post_button 	= $foreach_kq["button"];
            $post_link  	= $foreach_kq["url"];
        ?>
	        <div class="single-slider pt-140 pb-140 bg-img" style="background-image:url(<?php echo $post_image; ?>);">
	            <div class="container">
	                <div class="slider-content-4 slider-animated-1">
	                    <h5 class="animated"><?php echo $post_node; ?></h5>
	                    <h2 class="animated"><?php echo $post_title; ?></h2>
	                    <p class="animated"><?php echo $post_desc; ?></p>
	                    <div class="slider-btn-5 mt-70">
	                        <a class="animated" href="<?php echo $post_link; ?>"><?php echo $post_button; ?></a>
	                    </div>
	                </div>
	            </div>
	        </div>
        <?php } ?>

    </div>
</div>
<?php } ?>

<?php if(!empty( $home_ads_one )) { ?>
<div class="banner-area mt-30 madial-banner">
    <div class="container">
        <div class="row">

	        <?php
	        	$i=1;
	            foreach ($home_ads_one as $foreach_kq) {

	            $post_image = $foreach_kq["image"];
	            $post_link  = $foreach_kq["url"];
	        ?>
	            <div class="col-lg-6 col-md-6">
	                <div class="banner-img banner-hover <?php if($i%2==0) { echo 'mrg-top-sm'; } ?>">
	                    <a href="<?php echo $post_link; ?>">
	                    	<img alt="" src="<?php echo $post_image; ?>">
	                    </a>
	                </div>
	            </div>
	        <?php $i++; } ?>

        </div>
    </div>
</div>
<?php } ?>

<?php if(!empty( $home_tab_product )) { ?>
<div class="product-area pt-65 pb-30">
    <div class="container">
        <div class="text-uppercase product-tab-list tab-list-green mb-30 nav" role="tablist">

	        <?php
	        	$i = 1;
	            foreach ($home_tab_product as $foreach_kq) {

	            $post_title = $foreach_kq["title"];
	            $post_select_post = $foreach_kq["select_post"];
	        ?>
	            <a class="<?php if($i==1) { echo 'active'; } ?>" href="#home_<?php echo $i; ?>" data-toggle="tab">
	                <h4><?php echo $post_title; ?></h4>
	            </a>
	        <?php $i++; } ?>

        </div>
        <div class="tab-content jump">

	        <?php
	        	$i = 1;
	            foreach ($home_tab_product as $foreach_kq) {

	            $post_title = $foreach_kq["title"];
	            $post_select_post = $foreach_kq["select_post"];
	        ?>
	            <div class="tab-pane <?php if($i==1) { echo 'active'; } ?>" id="home_<?php echo $i; ?>">
	                <div class="custom-row">
	                    
	                    <?php if(!empty( $post_select_post )) { ?>
						<?php
						    foreach ($post_select_post as $foreach_kq) {

						    $post_id 			= $foreach_kq->ID;
							$post_title 		= get_the_title($post_id);
							$post_date 			= get_the_date('d/m/Y',$post_id);
							$post_link 			= get_permalink($post_id);
							$post_image 		= getPostImage($post_id,"p-product");
							$post_excerpt 		= cut_string(get_the_excerpt($post_id),200,'...');
							$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
							$post_tag 			= get_the_tags($post_id);

							$terms = wp_get_object_terms($post_id, 'product_cat');

						    // woocommerce
						    $product = new WC_product($post_id);
						    // wc gallery
						    $single_product_gallery = $product->get_gallery_image_ids();
						?>
		                    <div class="custom-col-5 mb-30">
		                        <div class="devita-product-2 devita-product-green mrg-inherit">
		                            <div class="product-img">
		                                <div class="product-img-slider">
							                <a href="<?php echo $post_link; ?>">
							                	<img src="<?php echo $post_image; ?>" alt="">
							                </a>

							                <?php if(!empty( $single_product_gallery )) { ?>
							                <?php
							                    foreach( $single_product_gallery as $single_product_gallery_kq ){

							                    $post_image = wp_get_attachment_url( $single_product_gallery_kq );
							                ?>
							                    <a href="<?php echo $post_link; ?>">
							                        <img src="<?php echo $post_image; ?>" alt="">
							                    </a>
							                <?php } ?>
							                <?php } ?>
		                                </div>
		                                <?php echo show_sale($post_id); ?>
		                            </div>
		                            <div class="list-col">
		                                <div class="gridview">
		                                    <div class="product-content text-center">

									            <?php if(!empty( $terms )) { ?>
									            <span class="cat-separator">
										            <?php
										                foreach ($terms as $foreach_kq) {

										                $term_id        = $foreach_kq->term_id;
										                $taxonomy_slug  = $foreach_kq->taxonomy;
										                $term_name      = get_term( $term_id, $taxonomy_slug )->name;

										                	echo $term_name.'<p>,</p> ';
										            	}
										            ?>
									            </span>
									            <?php } ?>

							                    <h4>
													<a title="<?php echo $post_title; ?>" href="<?php echo $post_link; ?>">
														<?php echo $post_title; ?>
													</a>
							                    </h4>
		                                        
		                                        <?php echo show_price_old_price($post_id); ?>
		                                    </div>
		                                    <div class="product-action-wrapper-2 text-center">
		                                        <p><?php echo $post_excerpt; ?></p>
		                                        <div class="product-action">
		                                            <?php echo show_add_to_cart_button_ajax($post_id); ?>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
						<?php } ?>
	                    <?php } ?>

	                </div>
	            </div>
	        <?php $i++; } ?>

        </div>
    </div>
</div>
<?php } ?>

<div class="best-selling-area pt-60 pb-45 bg-img" style="background-image:url(<?php echo asset('images/bg/bg-4.jpg'); ?>)">
    <div class="container">
        <div class="top-bar section-border mb-35">
            <div class="section-title-wrapper section-bg-gray">
                <h4 class="text-capitalize section-title"><?php echo $home_product_selling_title; ?></h4>
            </div>
            <div class="product-tab-list-2 nav section-bg-gray">

				<?php
					$i=1;
					foreach ($home_product_selling_select_cat as $foreach_kq) {

					$term_id 		= $foreach_kq->term_id;
					$taxonomy_slug 	= $foreach_kq->taxonomy;
					$term_name 		= get_term( $term_id, $taxonomy_slug )->name;
				?>
	                <a class="<?php if($i==1) { echo 'active'; } ?>" href="#home2_<?php echo $i; ?>" data-toggle="tab">
	                    <h4><?php echo $term_name; ?></h4>
	                </a>
				<?php $i++; } ?>

            </div>
        </div>
        <div class="tab-content jump">

			<?php
				$i=1;
				foreach ($home_product_selling_select_cat as $foreach_kq) {

				$term_id 		= $foreach_kq->term_id;
				$taxonomy_slug 	= $foreach_kq->taxonomy;
				$term_name 		= get_term( $term_id, $taxonomy_slug )->name;
			?>

	            <div class="tab-pane <?php if($i==1) { echo 'active'; } ?>" id="home2_<?php echo $i; ?>">
	                <div class="best-selling-active-2 nav-style nav-style-green">
                        <div class="best-selling-bundle-2">
                            <div class="row">

								<?php
									$y=1;
									$query = query_post_by_taxonomy_paged('product', $taxonomy_slug, $term_id, 6);

									if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

					                $post_id	= get_the_ID();
									$post_title 		= get_the_title($post_id);
									$post_date 			= get_the_date('d/m/Y',$post_id);
									$post_link 			= get_permalink($post_id);
									$post_image 		= getPostImage($post_id,"p-product");
									$post_excerpt 		= cut_string(get_the_excerpt($post_id),200,'...');
									$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
									$post_tag 			= get_the_tags($post_id);

									$terms = wp_get_object_terms($post_id, 'product_cat');

								    // woocommerce
								    $product = new WC_product($post_id);
								    // wc gallery
								    $single_product_gallery = $product->get_gallery_image_ids();
								?>
									
									<?php if($y==2) { echo '<div class="col-xl-4 col-lg-12 col-md-12 col-fix">'; } ?>
										
									<?php if($y==4) { echo '</div>'; } ?>

									<div class="col-xl-4 col-lg-12 col-md-12">
							            <div class="best-selling-pro-wrapper mb-20 devita-product-green <?php if($y==1){echo'best-selling-big-img';}?>">
							                <div class="product-img <?php if($y==1){echo'best-selling-img';}?>">
							                    <a href="<?php echo $post_link; ?>">
							                        <img src="<?php echo $post_image; ?>" alt="">
							                    </a>
							                    <?php echo show_sale($post_id); ?>
							                </div>
							                <div class="product-content best-pro-content <?php if($y==1){echo'best-selling-style-2';}?>">
									            <?php if(!empty( $terms )) { ?>
									            <span class="cat-separator">
										            <?php
										                foreach ($terms as $foreach_kq) {

										                $term_id        = $foreach_kq->term_id;
										                $taxonomy_slug  = $foreach_kq->taxonomy;
										                $term_name      = get_term( $term_id, $taxonomy_slug )->name;

										                	echo $term_name.'<p>,</p> ';
										            	}
										            ?>
									            </span>
									            <?php } ?>
							                    <h4>
													<a title="<?php echo $post_title; ?>" href="<?php echo $post_link; ?>">
														<?php echo $post_title; ?>
													</a>
							                    </h4>
							                    <?php echo show_price_old_price($post_id); ?>
							                    <?php if($y==1) { ?>
										            <div class="product-list">
										                <?php echo $post_excerpt; ?>
										            </div>
									            <?php } ?>
							                    <div class="product-action best-pro-action <?php if($y==1){echo'best-pro-action-2';}?>">
							                        <?php echo show_add_to_cart_button_ajax($post_id); ?>
							                    </div>
							                </div>
							            </div>
									</div>
										
								<?php $y++; endwhile; wp_reset_postdata(); else: echo ''; endif; ?>
                            </div>
                        </div>
	                </div>
	            </div>
            
            <?php $i++; } ?>

        </div>
    </div>
</div>

<?php if(!empty( $home_ads_two )) { ?>
<div class="banner-area madial-banner">
    <div class="container">
        <div class="row">

	        <?php
	        	$i=1;
	            foreach ($home_ads_two as $foreach_kq) {

	            $post_image = $foreach_kq["image"];
	            $post_link  = $foreach_kq["url"];
	        ?>
	            <div class="col-lg-6 col-md-6">
	                <div class="banner-img banner-hover <?php if($i%2==0) { echo 'mrg-top-sm'; } ?>">
	                    <a href="<?php echo $post_link; ?>">
	                    	<img alt="" src="<?php echo $post_image; ?>">
	                    </a>
	                </div>
	            </div>
	        <?php $i++; } ?>

        </div>
    </div>
</div>
<?php } ?>

<div class="blog-area pt-60 pb-60">
    <div class="container">
        <div class="section-border mb-35">
            <h4 class="section-title section-bg-white"><?php echo $home_news_title; ?></h4>
        </div>

        <?php if(!empty( $home_news_select_post )) { ?>
        <div class="blog-slider-active nav-style nav-style-green owl-carousel">

			<?php
			    foreach ($home_news_select_post as $foreach_kq) {

			    $post_id 			= $foreach_kq->ID;
				$post_title 		= get_the_title($post_id);
				$post_date 			= get_the_date('d',$post_id).' tháng '.get_the_date('m',$post_id).' năm '.get_the_date('Y',$post_id);
				$post_link 			= get_permalink($post_id);
				$post_image 		= getPostImage($post_id,"p-post");
				$post_excerpt 		= cut_string(get_the_excerpt($post_id),100,'...');
				$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
				$post_tag 			= get_the_tags($post_id);

				$get_category = get_the_category($post_id);
			?>

	            <div class="blog-wrapper">

		            <?php if(!empty( $get_category )) { ?>
		            <span class="cat-separator">
			            <?php
			                foreach ($get_category as $foreach_kq) {

			                $term_id        = $foreach_kq->term_id;
			                $taxonomy_slug  = $foreach_kq->taxonomy;
			                $term_name      = get_term( $term_id, $taxonomy_slug )->name;

			                	echo $term_name.'<p>,</p> ';
			            	}
			            ?>
		            </span>
		            <?php } ?>

	                <h3>
	                	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
	                		<?php echo $post_title; ?>
                		</a>
                	</h3>
	                <p><?php echo $post_excerpt; ?></p>
	                <div class="blog-meta-bundle">
	                    <div class="blog-meta">
	                        <ul>
	                            <li><?php echo $post_date; ?></li>
	                        </ul>
	                    </div>
	                    <div class="blog-readmore">
	                        <a href="<?php echo $post_link; ?>">Đọc thêm <i class="fa fa-angle-double-right"></i></a>
	                    </div>
	                </div>
	            </div>

			<?php } ?>

        </div>
        <?php } ?>
    </div>
</div>

<?php get_footer(); ?>

